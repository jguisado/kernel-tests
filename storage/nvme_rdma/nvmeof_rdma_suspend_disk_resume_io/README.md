# storage/nvme_rdma/nvmeof_rdma_suspend_disk_resume_io

Storage: Test suspend to disk and resume during io on rdma server

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
